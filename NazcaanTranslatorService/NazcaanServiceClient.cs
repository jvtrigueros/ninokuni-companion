﻿using System;
using System.IO;
using RestSharp;
using System.Windows.Media.Imaging;

namespace NazcaanTranslatorService
{
    public class NazcaanServiceClient
    {
        private string m_serverUrl = "http://kos-mos.zapto.org:3000";
        private RestClient m_restClient;

        public NazcaanServiceClient()
        {
            m_restClient = new RestClient(m_serverUrl);
        }

        public void processNazcaanImage(WriteableBitmap image, Action<string> nazcaanResult)
        {
            RestRequest translateRequest = new RestRequest("/translator", Method.POST);
            byte[] data = null;
            using (MemoryStream stream = new MemoryStream())
            {
                WriteableBitmap wBitmap = new WriteableBitmap(image);
                wBitmap.SaveJpeg(stream, wBitmap.PixelWidth, wBitmap.PixelHeight, 0, 100);
                stream.Seek(0, SeekOrigin.Begin);
                data = stream.GetBuffer();
            }

            translateRequest.AddFile("nazcaan_image", data, "nazcaan_image.jpg");

            m_restClient.ExecuteAsync(translateRequest, res =>
            {
                if (res.ResponseStatus == ResponseStatus.Error)
                    nazcaanResult("Could not process image.");
                else
                {
                    string trimmedContent = res.Content;
                    nazcaanResult(trimmedContent.Trim());
                }
            });
        }


    }
}
