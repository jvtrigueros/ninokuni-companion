﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Microsoft.Phone;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using NazcaanTranslatorService;

namespace NinoKuniInterface
{
    public partial class MainPage : PhoneApplicationPage
    {
        private Point start;
        private Point end;
        private Rectangle rectangle;
        private WriteableBitmap image;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            rectangle = new Rectangle() {
                Visibility = Visibility.Collapsed,
                Stroke = new SolidColorBrush(Colors.Black),
                StrokeThickness = 1
            };

            photoCanvas.Children.Add(rectangle);
            photoContainer.Width = photoCanvas.Width;
            photoContainer.Height = photoCanvas.Height;
        }

        private void CaptureImg_Click(object sender, EventArgs e)
        {
            var cameraTask = new CameraCaptureTask();
            cameraTask.Completed += cameraTask_Completed;
            cameraTask.Show();
        }

        private void LoadImg_Click(object sender, EventArgs e)
        {
            image = image.FromResource("TestImages/test1.jpg");
            //photoContainer.Source = new BitmapImage(new Uri("/ninokuni-interface;component/TestImages/test1.jpg",UriKind.Relative));
            photoContainer.Source = image;
            imageAddedSetup();
        }

        private void CropImg_Click(object sender, EventArgs e)
        {
            //rectangle.Visibility = Visibility.Collapsed;
            Debug.WriteLine(image.PixelWidth + "x" + image.PixelHeight);
            Debug.WriteLine(photoContainer.Width + ":" + photoContainer.ActualWidth);
            Debug.WriteLine(photoContainer.Height + ":" + photoContainer.ActualHeight);
            double widthRatio = image.PixelWidth / photoContainer.ActualWidth;
            double heightRatio = image.PixelHeight / photoContainer.ActualHeight;


            start.X *= widthRatio;
            end.X *= widthRatio;
            start.Y *= heightRatio;
            end.Y *= heightRatio;

            image = image.Crop(new Rect(start, end));
            photoContainer.Source = image;
        }

        private void TranslateImg_Click(object sender, EventArgs e)
        {
            NazcaanServiceClient nazClient = new NazcaanServiceClient();
            nazClient.processNazcaanImage(image, result => {
                Debug.WriteLine(result);
            });
        }


        void cameraTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                image.LoadJpeg(e.ChosenPhoto);
                //image = PictureDecoder.DecodeJpeg(e.ChosenPhoto);
                photoContainer.Source = image;
                Debug.WriteLine("Inserted Image");

                imageAddedSetup();
            }
        }

        private void imageAddedSetup()
        {
            photoContainer.Visibility = Visibility.Visible;
            photoBorder.Visibility = Visibility.Visible;

            photoContainer.MouseLeftButtonDown += new MouseButtonEventHandler(startPoint);
            photoContainer.MouseLeftButtonUp += new MouseButtonEventHandler(endPoint);
            photoContainer.MouseMove += new MouseEventHandler(middlePoint);

            rectangle.Visibility = Visibility.Visible;
        }
        void updateCropArea(object sender, EventArgs e)
        {
            rectangle.SetValue(Canvas.LeftProperty, start.X);
            rectangle.SetValue(Canvas.TopProperty, start.Y);
            rectangle.Width = end.X - start.X;
            rectangle.Height = end.Y - start.Y;
        }

        void startPoint(object sender, MouseButtonEventArgs e)
        {
            start = e.GetPosition(photoContainer);
            end = start;
            Debug.WriteLine("Start: " + start);
        }

        void middlePoint(object sender, MouseEventArgs e)
        {
            end = e.GetPosition(photoContainer);
            rectangle.SetValue(Canvas.LeftProperty, start.X);
            rectangle.SetValue(Canvas.TopProperty, start.Y);
            rectangle.Width = end.X - start.X;
            rectangle.Height = end.Y - start.Y;
        }

        void endPoint(object sender, MouseButtonEventArgs e)
        {
            end = e.GetPosition(photoContainer);
            Debug.WriteLine("End: " + end);
        }

    }
}